from hestia.crop_builder import FarmedCropBuilder

from data import DATA_DIR
from hestia.models.emissions.chemical.nh3_emissions import NH3Emissions
from hestia.models.emissions.chemical.no3_emissions import NO3Emissions
from hestia.models.emissions.chemical.nox_emissions import NOxEmissions
from hestia.models.emissions.chemical.n2o_emissions import N2OEmissions
from hestia.models.emissions.chemical.ch4_emissions import CH4Emissions
from hestia.models.emissions.chemical.co2_emissions import CO2Emissions
from hestia.models.emissions.chemical.p_emissions import PhosphorusEmissions
from hestia.models.emissions.activities.fertilizers_pesticide_and_other_inputs import FertilizerPesticideAndOtherInputsEmissions
from hestia.models.emissions.activities.irrigation_fuel_electricity import IrrigationFuelElectricityEmissions



from hestia.models.references.repository import ReferencesRepository

import pandas as pd
import numpy as np

if __name__ == '__main__':
    keynito = 17
    builder = FarmedCropBuilder()
    crop = builder.build_crop(keynito)

    database = pd.read_csv(DATA_DIR + "/database.csv", sep="|")

    crop.activities.fertilizing.organic.n = np.NAN if database.loc[keynito-1,"ML"] == "-" else float(database.loc[keynito-1,"ML"])
    crop.activities.fertilizing.organic.tan = np.NAN if database.loc[keynito-1,"MQ"] == "-" else float(database.loc[keynito-1,"MQ"])
    crop.activities.fertilizing.synthetic.n = np.NAN if database.loc[keynito-1,"MV"] == "-" else float(database.loc[keynito-1,"MV"])

    crop.activities.fertilizing.organic.p = np.NAN if database.loc[keynito-1,"NF"] == "-" else float(database.loc[keynito-1,"NF"])
    crop.activities.fertilizing.synthetic.p = np.NAN if database.loc[keynito-1,"NG"] == "-" else float(database.loc[keynito-1,"NG"])
    crop.activities.fertilizing.organic.k = np.NAN if database.loc[keynito-1,"NI"] == "-" else float(database.loc[keynito-1,"NI"])
    crop.activities.fertilizing.synthetic.k = np.NAN if database.loc[keynito-1,"NJ"] == "-" else float(database.loc[keynito-1,"NJ"])
    crop.activities.irrigation.energy.fuel_amount = np.NAN if database.loc[keynito-1,"NU"] == "-" else float(database.loc[keynito-1,"NU"])
    crop.activities.pest_management.pest1 = np.NAN if database.loc[keynito-1,"NX"] == "-" else float(database.loc[keynito-1,"NX"])

    references = ReferencesRepository()


    fert_pest_other_inputs_emissions = FertilizerPesticideAndOtherInputsEmissions(references)
    fert_pest_other_inputs_emissions.calculate_for(crop)

    irrigation_fuel_electricity_emissions = IrrigationFuelElectricityEmissions(references)
    irrigation_fuel_electricity_emissions.calculate_for(crop)

    nh3 = NH3Emissions(references)
    nh3.calculate_for(crop)

    nox = NOxEmissions(references)
    nox.calculate_for(crop)

    n2o = N2OEmissions(references)
    n2o.calculate_for(crop)

    no3 = NO3Emissions(references)
    no3.calculate_for(crop)

    ch4 = CH4Emissions(references)
    ch4.calculate_for(crop)

    co2 = CO2Emissions(references)
    co2.calculate_for(crop)

    p_emissions = PhosphorusEmissions(references)
    p_emissions.calculate_for(crop)


    print("\n")

    print("Fertilizer Pesticide And Other Inputs Emissions")

    print("\n")

    print(fert_pest_other_inputs_emissions.co2)
    print(fert_pest_other_inputs_emissions.so2)
    print(fert_pest_other_inputs_emissions.po4)

    print("\n")

    print("Irrigation Electricity and Fuel")

    print("\n")

    print()
    print(irrigation_fuel_electricity_emissions.co2)
    print(irrigation_fuel_electricity_emissions.so2)
    print(irrigation_fuel_electricity_emissions.po4)


    print("\n")

    print("Field Emissions")

    print("\n")

    print("N20 Emissions")
    print(n2o.total)
    print(n2o.synthetic)
    print(n2o.organic)
    print(n2o.excreta)
    print(n2o.residue_burn)

    print("CO2 Emissions")
    print(co2.urea)
    print(co2.lime)

    print("NH3 Emissions")
    print(nh3.synthetic)
    print(nh3.organic)
    print(nh3.excreta)
    print(nh3.residue)

    print("Nox Emissions")
    print(nox.total)
    print(nox.synthetic)
    print(nox.organic)
    print(nox.excreta)
    print(nox.residue)

    print("NO3 Emissions")
    print(no3.total)
    print(no3.synthetic)
    print(no3.organic)
    print(no3.excreta)
    print(no3.residue)

    print("CH4 Emissions")
    print(ch4.residue_burn)

    print("Phosphorus Emissions")
    print(p_emissions.R)
    print(p_emissions.P)
    print(p_emissions.A)
    print(p_emissions.Pe)
    print(p_emissions.Pr)
    print(p_emissions.Pd)
    print(p_emissions.Pg)
    print(p_emissions.PToWater)
    print(p_emissions.NErosion)




