import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= os.path.join(DATA_DIR, 'database.csv'),
    location_type='file',
    data_format='xslx',
    separator='|',
    id_key='id',
    column_names={
    'List_Climate_Zone':'id'
    'List_Climate_N2ON': 'n2o_n',
    'List_Climate_NOxN': 'nox_n',
        }
)
