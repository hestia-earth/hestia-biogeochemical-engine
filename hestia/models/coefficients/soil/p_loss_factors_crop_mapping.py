import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= DATA_DIR,
    location_type='directory',
    data_format='json',
    id_key='country',
    column_names={
        'List_C1Factors_Crops': 'crop',
        'List_C1Factors': 'c1',
        }
)
