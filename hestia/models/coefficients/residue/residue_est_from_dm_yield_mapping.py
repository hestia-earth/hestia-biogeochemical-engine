import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= os.path.join(DATA_DIR, 'database.csv'),
    location_type='file',
    data_format='xslx',
    separator='|',
    id_key='name',
    column_names={
        'List_CRes_Names': 'name',
        'List_CRes_Slope': 'slope',
        'List_CRes_Intercept': 'intercept',
        'List_CRes_N_AG': 'n_content_ag',
        'List_CRes_Ratio': 'ratio_ag_to_bg',
        'List_CRes_N_BG': 'n_content_bg',
        'List_CRes_CF': 'combustion'
        }
)
