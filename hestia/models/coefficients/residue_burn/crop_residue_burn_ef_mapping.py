import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= DATA_DIR,
    location_type='directory',
    data_format='json',
    id_key='name',
    column_names={
        'EF_Burn_DM_CH4': 'ch4',
        'EF_Burn_DM_N2O': 'n2o',
        'EF_Burn_DM_NH3': 'nh3',
        'EF_Burn_DM_NOx': 'nox',
        }
)
