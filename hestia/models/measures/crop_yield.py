from dataclasses import dataclass 


@dataclass
class CropYield:
    dry_matter: float
    marketable: float