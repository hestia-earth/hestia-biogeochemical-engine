from dataclasses import dataclass


@dataclass
class Energy:
    fuel_amount: float
    electricity: float

