from dataclasses import dataclass


@dataclass
class DryMatter:
    amount: float
    standard_deviation: float
    