from hestia.models.farmed_crop import FarmedCrop
from hestia.models.activities.irrigation.irrigation import Irrigation
from hestia.models.references.repository import ReferencesRepository

import numpy as np


class IrrigationFuelElectricityEmissions:
    co2: float
    so2: float
    po4: float

    def __init__(self, references_repository: ReferencesRepository):
        self._references = references_repository

    def calculate_for(self,crop: FarmedCrop):
        diesel_to_heat = self._references.get_diesel_heating()
        diesel_emissions = self._references.get_emissions_from_diesel()
        electricity_emissions = self._get_emissions_from_electricity_consumption(crop.field.land.country)

        self._calculate_so2(crop.activities.irrigation, diesel_to_heat, diesel_emissions, electricity_emissions)
        self._calculate_co2(crop.activities.irrigation, diesel_to_heat, diesel_emissions, electricity_emissions)
        self._calculate_po4(crop.activities.irrigation, diesel_to_heat, diesel_emissions, electricity_emissions)

    def _calculate_co2(self, irrigation: Irrigation, diesel_to_heat, diesel_emissions, electricity_emissions):
        self.co2 = (
            0 if np.isnan(irrigation.energy.fuel_amount) else irrigation.energy.fuel_amount / diesel_to_heat) \
                   * (diesel_emissions.loc['ghg','production'] + diesel_emissions.loc['ghg','combustion']) +\
                   (0 if np.isnan(irrigation.energy.electricity) else irrigation.energy.electricity * electricity_emissions['co2'])

    def _calculate_so2(self, irrigation: Irrigation, diesel_to_heat, diesel_emissions, electricity_emissions):
        self.so2 = (
            0 if np.isnan(irrigation.energy.fuel_amount) else irrigation.energy.fuel_amount / diesel_to_heat) \
                   * (diesel_emissions.loc['acid','production'] + diesel_emissions.loc['acid','combustion']) +\
                   (0 if np.isnan(irrigation.energy.electricity) else irrigation.energy.electricity * electricity_emissions['so2'])

    def _calculate_po4(self, irrigation: Irrigation, diesel_to_heat, diesel_emissions, electricity_emissions):
        self.po4 = (
            0 if np.isnan(irrigation.energy.fuel_amount) else irrigation.energy.fuel_amount / diesel_to_heat) \
                   * (diesel_emissions.loc['eutr','production'] + diesel_emissions.loc['eutr','combustion']) +\
                   (0 if np.isnan(irrigation.energy.electricity) else irrigation.energy.electricity * electricity_emissions['po4'])

    def _get_emissions_from_electricity_consumption(self, country: str):
        country_electricity_emissions = self._references.get_emissions_from_electricity_consumption_by_country()
        global_electricity_emissions = self._references.get_emissions_from_electricity_consumption_global()
        try:
            return country_electricity_emissions.loc[country]
        except:
            return global_electricity_emissions