import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= os.path.join(DATA_DIR, 'database.csv'),
    location_type='file',
    data_format='csv',
    separator='|',
    id_key='id',
    column_names={
        'AQ': 'id',
        'RF': 'type',
        'RG': 'glass',
        'RH': 'plastic',
        'RI': 'rockwool',
        'RJ': 'steel',
        'RK': 'aluminium',
        'RL': 'iron',
        'RM': 'concrete',
        'RN': 'wood',
        }
)
