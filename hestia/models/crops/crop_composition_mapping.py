import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= DATA_DIR,
    location_type='directory',
    data_format='json',
    id_key='name',
    column_names={
        'Table_Comp_Names': 'name',
        'Table_Comp_N': 'nitrogen_amount',
        'Table_Comp_GE': 'gross_energy',
        'Table_Comp_DM_Fresh': 'dm_at_harvest',
        'Table_Comp_DM_Dried': 'dm_marketable'
        }
)
