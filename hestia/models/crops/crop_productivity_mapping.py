import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= DATA_DIR,
    location_type='directory',
    data_format='json',
    separator='|',
    id_key='name',
    column_names={
        'Table_OrchardCrops_Crops': 'name',
        'Table_OrchardCrops_NursDur': 'nursery_duration',
        'Table_OrchardCrops_SapYield': 'sapling_yield',
        'Table_OrchardCrops_CultDur': 'cultiv_duration',
        'Table_OrchardCrops_OrchDens': 'orch_density',
        'Table_OrchardCrops_FallowDur': 'fallow_duration',
        'Table_OrchardCrops_NonBearDur': 'non_bear_duration'

        }
)
