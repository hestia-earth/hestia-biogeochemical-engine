from dataclasses import  dataclass

@dataclass()
class Pesticide:
    name: str
    amount: float