import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= os.path.join(DATA_DIR, 'database.csv'),
    location_type='file',
    data_format='csv',
    separator='|',
    id_key='id',
    column_names={
        'AQ': 'id',
        'NX': 'total_amount',
        'NY': 'pest1_name',
        'NZ': 'pest1_amount',
        'OA': 'pest2_name',
        'OB': 'pest2_amount',
        'OC': 'pest3_name',
        'OD': 'pest3_amount'
        }
)
