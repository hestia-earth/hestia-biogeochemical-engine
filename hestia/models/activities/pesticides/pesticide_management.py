from hestia.models.activities.pesticides.pesticide import Pesticide


class PesticideManagement:
    amount: float
    pest1: Pesticide
    pest2: Pesticide
    pest3: Pesticide
