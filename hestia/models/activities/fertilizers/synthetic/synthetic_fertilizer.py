from hestia.models.activities.fertilizers.synthetic.synthetic_fertilizer_composition import SyntheticFertilizerComposition


class SyntheticFertilizer():
    n: float
    p: float
    k: float
    composition: SyntheticFertilizerComposition