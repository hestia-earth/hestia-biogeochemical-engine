import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= os.path.join(DATA_DIR, 'database.csv'),
    location_type='file',
    data_format='csv',
    separator='|',
    id_key='id',
    column_names={
        'AQ': 'id',
        'MV': 'n_amount',
        'NG': 'p_amount',
        'NJ': 'k_amount',
        'MW': 'urea_uas',
        'MX': 'as',
        'MY': 'uan_solu',
        'MZ': 'an_acl_np_kn_npk',
        'NA': 'can',
        'NB': 'anha_aquaa',
        'NC': 'ap_dap_map',
        }
)
