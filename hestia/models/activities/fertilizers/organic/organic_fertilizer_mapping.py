import os
from data import DATA_DIR

MODEL_MAPPING=dict(
    location= os.path.join(DATA_DIR, 'database.csv'),
    location_type='file',
    data_format='csv',
    separator='|',
    id_key='id',
    column_names={
        'AQ': 'id',
        'ML': 'n_amount',
        'MQ': 'tan_amount',
        'NH': 'p_amount',
        'NJ': 'k_amount',
        'MM': 'n_liquid',
        'MN': 'n_solid',
        'MO': 'n_compost',
        'MP': 'n_manure',
        'MR': 'tan_liquid',
        'MS': 'tan_solid',
        'MT': 'tan_compost',
        'MU': 'tan_manure',
        }
)
