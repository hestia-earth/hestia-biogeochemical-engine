from .organic_fertilizer_composition import OrganicFertilizerComposition


class OrganicFertilizer:
    n: float
    tan: float
    p: float
    k: float
    n_composition: OrganicFertilizerComposition
    tan_composition: OrganicFertilizerComposition
